import org.junit.*;

import static org.junit.Assert.assertEquals;

public class TestHello {

    Hello hello;

    @Before
    public void init(){
        hello = new Hello();
    }

    @Test
    public void testWithHello(){
        assertEquals("Hello, World", hello.printf());
    }

    

}
